# Altstadt

### Soban

Zwingerstr. 21 Heidelberg, BW 69117

https://www.restaurant-soban.de


#### Pro

- good, spicy Korean food

#### Con

- not many vegetarian/vegan options
- quite small - best to make a reservation



